
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Iterator;
import java.util.Map;

/**
 * Servlet implementation class ContactsServlet
 */
@WebServlet("/ContactsServlet")
public class ContactsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String url = "jdbc:mysql://ec2bjablonski.ddns.net:3306/myDB";
	static String user = "appremoteuser";
	static String password = "password";
	static Connection connection = null;
	String stringJson = "{}";

	private final ObjectMapper mapper = new ObjectMapper();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContactsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return;
		}
		connection = null;
		try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
		} else {
			System.out.println("Failed to make connection!");
		}
		try {
			String selectSQL = "SELECT * FROM myTable";
			PreparedStatement preparedStatement = connection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();
			JSONArray jArr = new JSONArray();
			while (rs.next()) {
				String fname = rs.getString("firstName");
				String lname = rs.getString("lastName");
				String pnum = rs.getString("phoneNumber");
				String addr = rs.getString("address");

				JSONObject jObj = new JSONObject();
				jObj.put("First", fname);
				jObj.put("Last", lname);
				jObj.put("Number", pnum);
				jObj.put("Address", addr);
				jArr.add(jObj);
			}
			stringJson = mapper.writeValueAsString(jArr);
			response.getWriter().println(stringJson);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String data = buffer.toString();
		response.getWriter().println("Req contained: " + data);
		response.getWriter().println(request.getParameter("data"));
		String compacted = data.split("\"")[3]; // Data in A,B,C
		String[] data2 = compacted.split(",");
		String first = data2[0];
		String last = data2[1];
		String number = data2[2];
		String address = data2[3];

		response.getWriter().println("\n\n\n\n");
		response.getWriter().println("firstName=" + first);
		response.getWriter().println("lastName=" + last);
		response.getWriter().println("phoneNumber=" + number);
		response.getWriter().println("address=" + address);

		response.getWriter().println("\n\n\n\n");
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().println("-------- MySQL JDBC Connection Testing ------------<br>");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// System.out.println("Where is your MySQL JDBC Driver?");
			response.getWriter().println("Shit's broke yo");
			e.printStackTrace();
			return;
		}
		response.getWriter().println("MySQL JDBC Driver Registered!<br>");
		connection = null;
		try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
			response.getWriter().println("You made it, take control your database now!<br>");
		} else {
			System.out.println("Failed to make connection!");
		}
		try {
			String selectSQL = "INSERT INTO myTable (firstName, lastName, phoneNumber, address) VALUES (?,?,?,?)";
			response.getWriter().println("\n\n\n\n");
			response.getWriter().println("firstName=" + first);
			response.getWriter().println("lastName=" + last);
			response.getWriter().println("phoneNumber=" + number);
			response.getWriter().println("address=" + address);
			response.getWriter().println(selectSQL + "<br>");
			response.getWriter().println("------------------------------------------<br>");
			PreparedStatement preparedStatement = connection.prepareStatement(selectSQL);
			preparedStatement.setString(1, first);
			preparedStatement.setString(2, last);
			preparedStatement.setString(3, number);
			preparedStatement.setString(4, address);
			preparedStatement.executeUpdate();
			response.getWriter().println("Inserted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
